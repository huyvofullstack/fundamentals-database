create database QUANLYSINHVIEN
use QUANLYSINHVIEN

create table KHOA (
	MAKHOA varchar(20) primary key,
	TENKHOA varchar(20)
)

create table SINHVIEN (
	HOSV varchar(20),
	TENSV varchar(20),
	MASV varchar(20) primary key,
	NGAYSINH datetime,
	PHAI varchar(5),
	MAKHOA varchar(20) foreign key references KHOA(MAKHOA)
)

create table MONHOC (
	TENMH varchar(20),
	MAMH varchar(20) primary key,
	SOTIET int
)

create table KETQUA (
	MASV varchar(20) foreign key references SINHVIEN(MASV),
	MAMH varchar(20) foreign key references MONHOC(MAMH),
	LANTHI int,
	DIEM int,
	primary key(MASV, MAMH, LANTHI)
)


create proc cau1
(@HOSV varchar(20), @TENSV varchar(20), @MASV varchar(20), @NGAYSINH datetime, @PHAI varchar(5), @MAKHOA varchar(20))
as
begin
	insert into SINHVIEN values (@HOSV, @TENSV, @MASV, @NGAYSINH, @PHAI, @MAKHOA)
end

create proc cau2
(@HOSV varchar(20), @TENSV varchar(20), @MASV varchar(20), @NGAYSINH datetime, @PHAI varchar(5), @MAKHOA varchar(20))
as
begin
	if (@MAKHOA is null or exists (select MAKHOA from KHOA where KHOA.MAKHOA = @MAKHOA))
	if (@MASV in (select MASV from SINHVIEN)) print 'Duplicate primary key!'
	select @NGAYSINH from SINHVIEN where 2016-YEAR(@NGAYSINH) >= 18
	and 2016 - YEAR(@NGAYSINH) < 40
	insert into SINHVIEN values (@HOSV, @TENSV, @MASV, @NGAYSINH, @PHAI, @MAKHOA)
end


create proc cau3
(@HOSV varchar(20), @TENSV varchar(20), @MASV varchar(20), @NGAYSINH datetime, @PHAI varchar(5), @MAKHOA varchar(20))
as
begin
	if (@MAKHOA is null or exists (select MONHOC.MAMH, KETQUA.MASV from MONHOC, KETQUA where KETQUA.MASV = @MASV and KETQUA.MAMH = MONHOC.MAMH))
	insert into SINHVIEN values (@HOSV, @TENSV, @MASV, @NGAYSINH, @PHAI, @MAKHOA)
end

create proc cau4 (@MAKHOA varchar(20))
as
begin
	select count(MASV) from SINHVIEN where SINHVIEN.MAKHOA = @MAKHOA
end

create function cau5 (@MAKHOA varchar(20))
returns table
as return (select * from SINHVIEN where SINHVIEN.MAKHOA = @MAKHOA)

create function cau6 ()
returns int
as
begin
	declare @KETQUA int
	select @KETQUA = count(*) from SINHVIEN group by(SINHVIEN.MAKHOA)
	return @KETQUA
end

create function cau7 (@MASV varchar(20))
returns table
as return (select * from KETQUA where KETQUA.MASV = @MASV)

create function cau8 (@MAKHOA varchar(20))
returns int
as
begin
	declare @KETQUA int
	select @KETQUA = count(MASV) from SINHVIEN where SINHVIEN.MAKHOA = @MAKHOA
	return @KETQUA
end
