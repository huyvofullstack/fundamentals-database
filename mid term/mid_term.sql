﻿CREATE DATABASE NGK
USE NGK

CREATE TABLE LoaiNGK (
	Maloai VARCHAR(10) PRIMARY KEY,
	Tenloai NVARCHAR(30) UNIQUE
)

CREATE TABLE NGK (
	MaNGK VARCHAR(10) PRIMARY KEY,
	TenNGK NVARCHAR(30) UNIQUE,
	DVT NVARCHAR(10),
	Soluong INT,
	Dongia INT,
	MaloaiNGK VARCHAR(10) foreign key references LoaiNGK(Maloai),
	check (Soluong > 0 and Dongia > 0),
	check (DVT in (N'Chai', N'Lon', N'Thùng', N'Kết'))
)

CREATE TABLE Khachhang (
	MsKH VARCHAR(10) PRIMARY KEY,
	hoten NVARCHAR(50),
	diachi NVARCHAR(60),
	dienthoai NVARCHAR(30) default N'Chưa có'
)

CREATE TABLE Hoadon (
	Sohd varchar(10) PRIMARY KEY,
	MsKH VARCHAR(10) foreign key references Khachhang(MsKH),
	nhanvien NVARCHAR(30),
	ngaylap DATETIME default getDate()
)

CREATE TABLE CTHD (
	Sohd varchar(10),
	MaNGK VARCHAR(10),
	soluong INT check(soluong > 0),
	dongia INT,
	primary key(Sohd, MaNGK)
)

alter table CTHD add Thanhtien int
alter table CTHD add constraint FK_Sohd foreign key (Sohd) references Hoadon(Sohd)
alter table CTHD add constraint FK_MaNGK foreign key(MaNGK) references NGK(MaNGK)
alter table CTHD add constraint CK_Dongia check (Dongia > 1000)


update CTHD
set Thanhtien = soluong * dongia * case
when MaNGK = '001' then 0.95
else 1
end


update NGK set Dongia = case
when Dongia + 5000 > 50000 then 50000
else Dongia + 5000
end
where MaNGK = '002'


select a.MaNGK, a.TenNGK, a.MaLoaiNGK from NGK a, Hoadon b, CTHD c
where a.MaNGK = c.MaNGK and c.Sohd = b.Sohd
and month(b.ngaylap) > 3 and year(b.ngaylap) = 2016


select NGK.MaNGK, TenNGK, sum(Thanhtien) as TongTien
from NGK, CTHD
where NGK.MaNGK = CTHD.MaNGK
group by NGK.MaNGK, TenNGK


select * from Khachhang where MsKH in
(select count(MsKH) from Hoadon group by MsKH having count(MsKH) >=
(select top 1 count(MsKH) from Hoadon group by MsKH order by count(MsKH) desc))


select * from NGK where MaNGK in
(select MaNGK from CTHD group by MaNGK having sum(soluong) >=
(select top 1 sum(soluong) from CTHD group by MaNGK order by sum(soluong) desc))
