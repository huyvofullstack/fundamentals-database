create database QLCHSACH
use QLCHSACH

create table NhomSach (
	MaNhom char(5),
	TenNhom nvarchar(25)
)

create table NhanVien (
	MaNV char(5),
	HoLot nvarchar(25),
	TenNV nvarchar(10),
	Phai nvarchar(3),
	NgaySinh smalldatetime,
	DiaChi nvarchar(40)
)

create table DanhMucSach (
	MaSach char(5),
	TenSach nvarchar(40),
	TacGia nvarchar(20),
	MaNhom char(5),
	DonGia numeric(5),
	SLTon numeric(5)
)

create table HoaDon (
	MaHD char(5),
	NgayBan smalldatetime,
	MaNV char(5)
)

create table ChiTietHoaDon (
	MaHD char(5),
	MaSach char(5),
	SoLuong numeric(5)
)

create table HoaDon_Luu (
	MaHD char(5),
	NgayBan smalldatetime,
	MaNV char(5)
)


create trigger cau1
on NhomSach
for insert
as
	declare @num int
	set @num = count(*) from inserted
begin
	print 'Co ' + cast(@num as varchar(5)) + ' mau tin duoc chen'
	rollback tran
end

create trigger cau2
on HoaDon, HoaDon_Luu
for insert
as
	declare @num int
	set @num = count(*) from inserted
begin
	insert into HoaDon_Luu select * from inserted
	print 'Co ' + cast(@num as varchar(5)) + ' mau tin duoc chen'
	rollback
end

create trigger cau3_a
on ChiTietHoaDon
for insert
as
	declare 
