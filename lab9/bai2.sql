create database QLTHUVIEN
use QLTHUVIEN

create table DocGia (
	ma_DocGia varchar(10),
	ho varchar(10),
	tenlot varchar(10),
	ten varchar(10),
	ngaysinh datetime
)

create table Nguoilon (
	ma_DocGia varchar(10),
	sonha int,
	duong varchar(25),
	quan int,
	dienthoai numeric(10),
	han_sd datetime
)

create table Treem (
	ma_DocGia varchar(10),
	ma_DocGia_NguoiLon varchar(10)
)

create table Tuasach (
	ma_tuasach varchar(10),
	tuasach varchar(10),
	tacgia varchar(10),
	tomtat text
)

create table Dausach (
	isbn varchar(10),
	ma_tuasach varchar(10),
	ngonngu varchar(10),
	bia varchar(10),
	trangthai varchar(10)
)

create table Cuonsach (
	isbn varchar(10),
	ma_cuonsach varchar(10),
	tinhtrang varchar(10)
)


