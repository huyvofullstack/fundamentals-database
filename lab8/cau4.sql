create database qlsv1
use qlsv1

create table lop (
	malop varchar(10) primary key,
	tenlop nvarchar(20)
)
create table sinhvien (
	masv varchar (10) primary key,
	hoten nvarchar(30),
	ngaysinh datetime,
	malop varchar (10) foreign key references lop(malop)
)
create table monhoc (
	mamh varchar(10) primary key,
	tenmh nvarchar (30)
)
create table ketqua (
	masv varchar(10),
	mamh varchar(10),
	diem float,
	primary key (masv, mamh),
	foreign key (masv) references sinhvien(masv),
	foreign key (mamh) references monhoc (mamh)
)

insert into lop values ('TH01', N'L?p tin h?c 01')
insert into lop values ('TH02', N'L?p tin h?c 02')
insert into lop values ('DT01', N'L?p di?n t? 01')

insert into sinhvien values ('TH01001', N'Nguy?n Van H�ng', '3/3/1990', 'TH01')
insert into sinhvien values ('TH01002', N'Nguy?n Van H�ng', '3/3/1990', 'TH01')
insert into sinhvien values ('DT01001', N'Nguy?n Van H�ng', '3/3/1990', 'DT01')

insert into monhoc values ('T', N'To�n')
insert into monhoc values ('L', N'L�')
insert into monhoc values ('H', N'Ho�')

insert into ketqua values ('TH01001','T',10)
insert into ketqua values ('TH01001','L',8.5)
insert into ketqua values ('TH01002','T',7.4)
insert into ketqua values ('DT01001','L',8)

-- 3a
create function caua(@ml varchar(10))
returns table
as
	return (
		select (UPPER(left(hoten,1))) + substring(hoten,2,len(hoten)-1) as hoten, convert(varchar(10),ngaysinh,103) as Ngaysinh
		from sinhvien
		where malop = @ml
	)
go
-- drop function caua
select * from caua ('TH01')

-- 3b
create function caub()
returns table
as
	return (
		select masv, mamh, str(diem,5,2) as diem
		from ketqua
	)
go
select * from caub()

-- 3c
create function cauc()
returns table
as
	return (
		select hoten, ngaysinh, datename(dw,ngaysinh) as Thu
		from sinhvien
	)
go
select * from cauc()

-- 3d
create function caud()
returns table
as
	return (select masv, reverse(substring(reverse(hoten),charindex(' ',reverse(hoten))+1, len(hoten)-len(left(reverse(hoten),charindex(' ', reverse(hoten)))))) as HoLot,
	reverse(left(reverse(hoten),charindex(' ',reverse(hoten)))) as ten
	from sinhvien)
go
select * from caud()

-- 3e
drop function caue
create function caue(@ml varchar(10))
returns varchar(10)
as
begin
	return (
		select top 1 masv
		from sinhvien
		where malop = @ml
		order by masv desc
	)
end
go
-- drop function cau1
print dbo.caue('TH01')

-- 3f
create proc cauf @ht nvarchar(30), @ns datetime, @ml varchar(10)
as
	declare @masv varchar(10)
	declare @stt int
	set @masv = dbo.caue(@ml)
	set @stt = cast(right(@masv, 3) as int) + 1
	
	if @stt < 10
	begin
		set @masv = @ml + '00' + cast(@stt as varchar(1))
	end
	else if @stt < 100
	begin
		set @masv = @ml + cast(@stt as varchar(3))
	end
	
	insert into sinhvien values (@masv, @ht, @ns, @ml)
go
-- drop proc cauf
exec cauf 'Nguyen Lam', '3/30/1990', 'TH01'

-- 3g
create function caug()
returns table
as
	return (
		select sinhvien.masv, hoten, str(avg(diem),5,1) as dtb
		from sinhvien, ketqua where sinhvien.masv = ketqua.masv
		group by sinhvien.masv, hoten
		having avg(diem) >= 5
	)
go
select * from caug()


-- 4a
create function cau4a()
returns table
as
	return (
		select hoten from sinhvien
	)
go
select * from cau4a()

-- 4b
create function cau4b()
returns table
as
	return (
		select hoten, ngaysinh, datename(dw,ngaysinh) as Thu
		from sinhvien
	)
go
select * from cau4b()

-- 4c
create function cau4c()
returns table
as
	return (
		select sinhvien.masv, sinhvien.hoten, year(sinhvien.ngaysinh) as namsinh, str(avg(ketqua.diem),5,1) as dtb
		from sinhvien, ketqua
		where sinhvien.masv = ketqua.masv
		group by sinhvien.masv, sinhvien.hoten, year(sinhvien.ngaysinh)
		having avg(diem) < 5
	)
go
select * from cau4c()

-- 4d
create function cau4d(@masv varchar(10))
returns table
as
	return (
		select a.malop, a.tenlop
		from lop a, sinhvien b
		where b.masv = @masv and b.malop = a.malop
	)
go
select * from cau4d('TH01001')

-- 4e
create function cau4e(@mamh varchar(10))
returns table
as
	return (
		select a.tenmh, str(avg(b.diem),5,2) as dtb
		from monhoc a, ketqua b
		where a.mamh = @mamh and a.mamh = b.mamh
		group by a.tenmh
	)
go
select * from cau4e('H')

-- 4f
drop function cau4f
create function cau4f(@ml varchar(10))
returns table
as
	return (
		select top 1 str(avg(diem),5,2) as dtb
		from ketqua
		where @ml = left(masv,4)
		group by masv
		order by avg(diem) desc
	)
go
select * from cau4f('TH01')
