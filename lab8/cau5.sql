create database NGK
use NGK

create table NSX (
	maNSX varchar(10) primary key,
	tenNSX varchar(10)
)

create table NGK (
	MaNGK varchar(10) primary key,
	TenNGK varchar(10),
	DVT varchar(10),
	soluong int,
	dongia int,
	Maloai varchar(10)
)

create table Hoadon (
	sohd varchar(10) primary key,
	ngaylap datetime
)

create table CTHD (
	sohd varchar(10),
	MaNGK varchar(10),
	soluong int,
	dongia int,
	primary key (sohd, MaNGK),
	foreign key (sohd) references Hoadon(sohd),
	foreign key (MaNGK) references NGK(MaNGK)
)

-- cau a
drop proc caua
create proc caua @ngaylap datetime
as
	declare @sohd varchar (10)
	declare @stt int
	set @stt = 1
	if not exists(select 1 from Hoadon)
	begin
		set @sohd = cast(day(@ngaylap) as varchar(2))
					+ cast(month(@ngaylap) as varchar(2))
					+ right(cast(year(@ngaylap) as varchar(4)),2)
					+ cast(@stt as varchar(1))
		set @stt = @stt + 1
	end
	else if exists(select 1 from Hoadon)
	begin
		set @sohd = cast(day(@ngaylap) as varchar(2))
					+ cast(month(@ngaylap) as varchar(2))
					+ right(cast(year(@ngaylap) as varchar(4)),2)
					+ cast(@stt as varchar(1))
		set @stt = @stt + 1
	end

	insert into Hoadon values (@sohd, @ngaylap)
go

delete from Hoadon
exec caua '3/3/1996'

-- cau b
create proc caub
as
	declare @ngaylap datetime
	declare @sohd varchar(10)
	declare @stt int
	set @stt = 1
	set @ngaylap = getdate()

	if not exists(select 1 from Hoadon)
	begin
		set @sohd = cast(day(@ngaylap) as varchar(2))
					+ cast(month(@ngaylap) as varchar(2))
					+ right(cast(year(@ngaylap) as varchar(4)),2)
					+ cast(@stt as varchar(1))
		set @stt = @stt + 1
	end
	else if exists(select 1 from Hoadon)
	begin
		set @sohd = cast(day(@ngaylap) as varchar(2))
					+ cast(month(@ngaylap) as varchar(2))
					+ right(cast(year(@ngaylap) as varchar(4)),2)
					+ cast(@stt as varchar(1))
		set @stt = @stt + 1
	end

	insert into Hoadon values (@sohd, @ngaylap)
go

exec caub
select * from Hoadon

-- cau c
create proc cauc @TenNGK varchar(10), @DVT varchar(10), @soluong int, @dongia int, @Maloai varchar(10), @maNSX varchar(10)
as
	declare @MaNGK varchar(10)
	declare @stt varchar(1)
	set @stt = '1'
	if @Maloai = 'COCA'
	begin
		if not exists(select 1 from NGK)
		begin
			set @MaNGK = @maNSX + '00' + @stt
		end
		else if exists(select 1 from NGK)
		begin
			set @MaNGK = @maNSX + '00' + @stt
		end
	end

	insert into NGK values (@MaNGK, @TenNGK, @DVT, @soluong, @dongia, @Maloai)
go

exec cauc 'A', 'vnd', 5, 10000, 'COCA', 'A1'
select * from NGK

-- cau d
create proc caud @sohd varchar(10), @MaNGK varchar(10), @soluong int, @dongia int, @NGKsoluong int, @NGKdongia int
as
	if @soluong > @NGKsoluong
	begin
		set @soluong = @NGKsoluong
	end
	set @dongia = @NGKdongia*1.5

	insert into CTHD values (@sohd, @MaNGK, @soluong, @dongia)
go

exec caud '33961', 'A1001', 6, 7000, 5, 8000
select * from CTHD

-- cau e
drop function caue
create function caue(@sohd varchar(10))
returns table
as
	return (
		select soluong*dongia as tongtien
		from CTHD
		where sohd = @sohd 
	)
go
select * from caue('33961')

-- cau f
create function cauf()
returns table
as
	return (
		select * from NGK
		where MaNGK in (
			select MaNGK from CTHD
			where sohd in (
				select sohd from Hoadon
				where month(ngaylap) = 3 and year(ngaylap) = 2016
			)
		)
	)
go
select * from cauf()

