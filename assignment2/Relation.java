import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class Relation {

	private List<String> attributes;
	private List<String> superkeys;
	private List<FunctionalDependency> functionalDependencies;

	private Relation(
		List<String> attributes, 
		List<FunctionalDependency> functionalDependencies,
		List<String> superkeys
	) {
		this.setAttributes(attributes);
		this.setFunctionalDependencies(functionalDependencies);
		this.setSuperkeys(superkeys);
	}

	public static Relation newRelation(
		List<String> attributes,
		List<FunctionalDependency> functionalDependencies,
		List<String> superkeys
	) {
		return new Relation(attributes, functionalDependencies, superkeys);
	}

	public static Relation newRelationWith(FunctionalDependency f) {
		List<String> attributes = new ArrayList<>();
		List<String> superkeys = new ArrayList<>();
		List<FunctionalDependency> functionalDependencies = new ArrayList<>();

		attributes.add(f.leftSideToString());
		attributes.add(f.rightSideToString());
		superkeys.add(f.leftSideToString());
		functionalDependencies.add(f);

		return new Relation(attributes, functionalDependencies, superkeys);
	}

	public static Relation newRelationWith(List<FunctionalDependency> list) {
		List<String> attributes = new ArrayList<>();
		List<String> superkeys = new ArrayList<>();
		List<FunctionalDependency> functionalDependencies = new ArrayList<>();

		superkeys.add(list.get(0).leftSideToString());
		attributes.add(list.get(0).leftSideToString());
		for (FunctionalDependency f : list) {
			functionalDependencies.add(f);
			attributes.add(f.rightSideToString());
		}

		return new Relation(attributes, functionalDependencies, superkeys);
	}

	public void setAttributes(List<String> attributes) {
		this.attributes = attributes;
	}

	public List<String> getAttributes() {
		return this.attributes;
	}

	public void setFunctionalDependencies(List<FunctionalDependency> functionalDependencies) {
		this.functionalDependencies = functionalDependencies;
	}

	public List<FunctionalDependency> getFunctionalDependencies() {
		return this.functionalDependencies;
	}

	public void setSuperkeys(List<String> superkeys) {
		this.superkeys = superkeys;
	}

	public List<String> getSuperkeys() {
		return this.superkeys;
	}

	public int readTestCase(String filename) {
		String[] str;
		FileInputStream in = null;
		try {
            in = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Scanner scanner = new Scanner(in);
        for (int i = 0; i < 3; i++) {
        	if (i == 0) {
        		str = scanner.nextLine().split("::");
        		for (String item : str) {
        			this.getAttributes().add(item);
        		}
        	}
        	else if (i == 1) {
        		str = scanner.nextLine().split("::");
        		for (String item : str) {
        			this.getSuperkeys().add(item);
        		} 
        	}
        	else {
        		str = scanner.nextLine().split("::");
        		for (String item : str) {
        			String[] FD = item.split("->");
					List<String> leftHandSide = new ArrayList<>();
					List<String> rightHandSide = new ArrayList<>();
					String[] LHS = FD[0].split("");
					String[] RHS = FD[1].split("");
					for (String name : LHS) {
						leftHandSide.add(name);
					}
					for (String name : RHS) {
						rightHandSide.add(name);
					}
					FunctionalDependency f = new FunctionalDependency(leftHandSide, rightHandSide);
					this.getFunctionalDependencies().add(f);
        		}
        	}
        }
        return 1;
	}

	public boolean check_2NF() {
		for (String key : superkeys) {
			for (FunctionalDependency f : functionalDependencies) {
				String leftHandSide = f.leftSideToString();
				if (!key.equals(leftHandSide) && key.contains(leftHandSide)) {
					return false;
				}	
			}
		}
		return true;
	}

	public FunctionalDependency getFD(String key) {
		for (FunctionalDependency f : functionalDependencies) {
			if (f.leftSideToString().equals(key)) {
				return f;
			}
		}
		return null;
	}

	public boolean check_3NF() {
		for (String key : superkeys) {
			for (FunctionalDependency f : functionalDependencies) {
				if (!f.leftSideToString().equals(key)) {
					return false;
				}
			}
		}
		return true;
	}

	public int removeAttribute(String attr) {
		for (String item : attributes) {
			if (item.equals(attr)) {
				attributes.remove(item);
				break;
			}
		}
		return 1;
	}

	public boolean isIn(String leftHandSide, List<Relation> list) {
		for (Relation r : list) {
			if (leftHandSide.equals(r.getSuperkeys().get(0))) {
				return true;
			}
		}
		return false;
	}

	public List<Relation> splitInto(String option) {
		List<Relation> list = new ArrayList<>();
		List<FunctionalDependency> toBeRemove = new ArrayList<>();
		List<FunctionalDependency> temp = new ArrayList<>();

		for (String key : superkeys) {

			for (FunctionalDependency f : functionalDependencies) {

				String leftHandSide = f.leftSideToString();

				if (option.equals("2NF")) {
					if (!key.equals(leftHandSide) && key.contains(leftHandSide) && !isIn(leftHandSide, list)) {
						for (FunctionalDependency f2 : functionalDependencies) {
							if (f2.leftSideToString().equals(leftHandSide)) {
								toBeRemove.add(f2);
								temp.add(f2);
								removeAttribute(f2.rightSideToString());
							}
						}
						Relation r = Relation.newRelationWith(temp);
						list.add(r);
						temp.clear();
					}
				}
				else if (option.equals("3NF")) {
					if (!key.equals(leftHandSide) && !isIn(leftHandSide, list)) {
						for (FunctionalDependency f2 : functionalDependencies) {
							if (f2.leftSideToString().equals(leftHandSide)) {
								toBeRemove.add(f2);
								temp.add(f2);
								removeAttribute(f2.rightSideToString());
							}
						}
						Relation r = Relation.newRelationWith(temp);
						list.add(r);
						temp.clear();
					}
				}

			}

		}

		for (FunctionalDependency f : toBeRemove) {
			functionalDependencies.remove(f);
		}

		return list;
	}

	public void updateAttributes() {
		for (FunctionalDependency f : functionalDependencies) {
			if (!this.getAttributes().toString().contains(f.rightSideToString())) {
				this.getAttributes().add(f.rightSideToString());
			}
		}
	}

	public void normalize() {
		List<Relation> list = new ArrayList<>();
		List<Relation> list2 = new ArrayList<>();

		if (this.check_2NF() == false) {
			System.out.println("This relation is not in 2NF!");
			list = splitInto("2NF");
			this.updateAttributes();
			list.add(this);
			System.out.println("Optimized to 2NF!");
			System.out.println();
		}
		else {
			System.out.println("This relation is in 2NF!");
			list.add(this);
		}

		if (list.get(list.size()-1).check_3NF() == false) {
			System.out.println("This relation is not in 3NF!");
			list2 = list.get(list.size()-1).splitInto("3NF");
			this.updateAttributes();
			list.addAll(list2);
			System.out.println("Optimized to 3NF!");
			System.out.println();
		}
		else {
			System.out.println("This relation is in 3NF!");
			System.out.println();
		}

		printAll(list);
	}

	public void printAll(List<Relation> list) {
		System.out.println("Your final result: ");
		for (Relation r : list) {
			System.out.println();
			r.print();
		}
	}

	public void print() {
		System.out.println("Attributes: " + this.getAttributes().toString());
		System.out.println("Keys: " + this.getSuperkeys().toString());
		System.out.println("Functional dependencies:");
		this.getFunctionalDependencies().forEach(item -> System.out.println(item.toString()));
	}

}
