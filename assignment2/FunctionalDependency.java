import java.util.List;


public class FunctionalDependency {

	private List<String> leftSide;
	private List<String> rightSide;

	public FunctionalDependency(List<String> leftSide, List<String> rightSide) {
		this.setLeftSide(leftSide);
		this.setRightSide(rightSide);
	}

	public void setLeftSide(List<String> leftSide) {
		this.leftSide = leftSide;
	}

	public List<String> getLeftSide() {
		return this.leftSide;
	}

	public void setRightSide(List<String> rightSide) {
		this.rightSide = rightSide;
	}

	public List<String> getRightSide() {
		return this.rightSide;
	}

	public String toString() {
		String result = "";
		for (String item : leftSide) {
			result += item;
		}
		result += "->";
		for (String item : rightSide) {
			result += item;
		}
		return result;
	}

	public String leftSideToString() {
		String result = "";
		for (String item : leftSide) {
			result += item;
		}
		return result;
	}

	public String rightSideToString() {
		String result = "";
		for (String item : rightSide) {
			result += item;
		}
		return result;
	}

}
