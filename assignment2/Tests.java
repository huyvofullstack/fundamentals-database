import java.io.Console;
import java.util.ArrayList;
import java.util.List;


public class Tests {

	public static void clearScreen() {  
   		System.out.print("\033[H\033[2J");  
    	System.out.flush();  
   	}  

	public static void main(String[] args) {
		clearScreen();

		Relation R = Relation.newRelation(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
		Console console = System.console();
		
		System.out.println("Creating a Relation...");

		for (String arg : args) {
			R.readTestCase(arg);
		}

		System.out.println("Your relation is:\n");
		R.print();
		System.out.println();

		R.normalize();
	}

}
