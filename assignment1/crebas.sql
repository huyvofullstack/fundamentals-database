/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 11                       */
/* Created on:     29/04/2016 10:00:03 SA                       */
/*==============================================================*/


if exists(select 1 from sys.sysforeignkey where role='FK_CHUYENBA_RELATIONS_NHANVIEN') then
    alter table ChuyenBay
       delete foreign key FK_CHUYENBA_RELATIONS_NHANVIEN
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_CHUYENBA_C_MAYBAY') then
    alter table ChuyenBay
       delete foreign key FK_CHUYENBA_C_MAYBAY
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_GIAYCHUN_RELATIONS_NHANVIEN') then
    alter table GiayChungNhan
       delete foreign key FK_GIAYCHUN_RELATIONS_NHANVIEN
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_GIAYCHUN_CO_LOAIMB') then
    alter table GiayChungNhan
       delete foreign key FK_GIAYCHUN_CO_LOAIMB
end if;

if exists(select 1 from sys.sysforeignkey where role='FK_MAYBAY_D_LOAIMB') then
    alter table MayBay
       delete foreign key FK_MAYBAY_D_LOAIMB
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_5_FK'
     and t.table_name='ChuyenBay'
) then
   drop index ChuyenBay.Relationship_5_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='c_FK'
     and t.table_name='ChuyenBay'
) then
   drop index ChuyenBay.c_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='ChuyenBay_PK'
     and t.table_name='ChuyenBay'
) then
   drop index ChuyenBay.ChuyenBay_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='ChuyenBay'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table ChuyenBay
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_7_FK'
     and t.table_name='GiayChungNhan'
) then
   drop index GiayChungNhan.Relationship_7_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='co_FK'
     and t.table_name='GiayChungNhan'
) then
   drop index GiayChungNhan.co_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='GiayChungNhan_PK'
     and t.table_name='GiayChungNhan'
) then
   drop index GiayChungNhan.GiayChungNhan_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='GiayChungNhan'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table GiayChungNhan
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='LoaiMB_PK'
     and t.table_name='LoaiMB'
) then
   drop index LoaiMB.LoaiMB_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='LoaiMB'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table LoaiMB
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='d_FK'
     and t.table_name='MayBay'
) then
   drop index MayBay.d_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='MayBay_PK'
     and t.table_name='MayBay'
) then
   drop index MayBay.MayBay_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='MayBay'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table MayBay
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='NhanVien_PK'
     and t.table_name='NhanVien'
) then
   drop index NhanVien.NhanVien_PK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='NhanVien'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table NhanVien
end if;

/*==============================================================*/
/* Table: ChuyenBay                                             */
/*==============================================================*/
create table ChuyenBay 
(
   SoHieuCB             varchar(10)                    not null,
   SoHieuMB             varchar(10)                    null,
   MaNV                 varchar(10)                    null,
   DiemXuatPhat         long varchar                   null,
   DiemDen              long varchar                   null,
   KhoangCach           decimal(19,4)                  null,
   GioKhoiHanh          timestamp                      null,
   GioHaCanh            timestamp                      null,
   constraint PK_CHUYENBAY primary key (SoHieuCB)
);

/*==============================================================*/
/* Index: ChuyenBay_PK                                          */
/*==============================================================*/
create unique index ChuyenBay_PK on ChuyenBay (
SoHieuCB ASC
);

/*==============================================================*/
/* Index: c_FK                                                  */
/*==============================================================*/
create index c_FK on ChuyenBay (
SoHieuMB ASC
);

/*==============================================================*/
/* Index: Relationship_5_FK                                     */
/*==============================================================*/
create index Relationship_5_FK on ChuyenBay (
MaNV ASC
);

/*==============================================================*/
/* Table: GiayChungNhan                                         */
/*==============================================================*/
create table GiayChungNhan 
(
   MaGCN                varchar(10)                    not null,
   MaLoaiMB             varchar(10)                    null,
   MaNV                 varchar(10)                    null,
   constraint PK_GIAYCHUNGNHAN primary key (MaGCN)
);

/*==============================================================*/
/* Index: GiayChungNhan_PK                                      */
/*==============================================================*/
create unique index GiayChungNhan_PK on GiayChungNhan (
MaGCN ASC
);

/*==============================================================*/
/* Index: co_FK                                                 */
/*==============================================================*/
create index co_FK on GiayChungNhan (
MaLoaiMB ASC
);

/*==============================================================*/
/* Index: Relationship_7_FK                                     */
/*==============================================================*/
create index Relationship_7_FK on GiayChungNhan (
MaNV ASC
);

/*==============================================================*/
/* Table: LoaiMB                                                */
/*==============================================================*/
create table LoaiMB 
(
   MaLoaiMB             varchar(10)                    not null,
   TenLoai              varchar(10)                    null,
   constraint PK_LOAIMB primary key (MaLoaiMB)
);

/*==============================================================*/
/* Index: LoaiMB_PK                                             */
/*==============================================================*/
create unique index LoaiMB_PK on LoaiMB (
MaLoaiMB ASC
);

/*==============================================================*/
/* Table: MayBay                                                */
/*==============================================================*/
create table MayBay 
(
   SoHieuMB             varchar(10)                    not null,
   MaLoaiMB             varchar(10)                    null,
   Ten                  varchar(30)                    null,
   DungTich             integer                        null,
   constraint PK_MAYBAY primary key (SoHieuMB)
);

/*==============================================================*/
/* Index: MayBay_PK                                             */
/*==============================================================*/
create unique index MayBay_PK on MayBay (
SoHieuMB ASC
);

/*==============================================================*/
/* Index: d_FK                                                  */
/*==============================================================*/
create index d_FK on MayBay (
MaLoaiMB ASC
);

/*==============================================================*/
/* Table: NhanVien                                              */
/*==============================================================*/
create table NhanVien 
(
   MaNV                 varchar(10)                    not null,
   Ho                   long varchar                   null,
   Ten                  varchar(30)                    null,
   Luong                decimal(19,4)                  null,
   DiaChi               varchar(50)                    null,
   constraint PK_NHANVIEN primary key (MaNV)
);

/*==============================================================*/
/* Index: NhanVien_PK                                           */
/*==============================================================*/
create unique index NhanVien_PK on NhanVien (
MaNV ASC
);

alter table ChuyenBay
   add constraint FK_CHUYENBA_RELATIONS_NHANVIEN foreign key (MaNV)
      references NhanVien (MaNV)
      on update restrict
      on delete restrict;

alter table ChuyenBay
   add constraint FK_CHUYENBA_C_MAYBAY foreign key (SoHieuMB)
      references MayBay (SoHieuMB)
      on update restrict
      on delete restrict;

alter table GiayChungNhan
   add constraint FK_GIAYCHUN_RELATIONS_NHANVIEN foreign key (MaNV)
      references NhanVien (MaNV)
      on update restrict
      on delete restrict;

alter table GiayChungNhan
   add constraint FK_GIAYCHUN_CO_LOAIMB foreign key (MaLoaiMB)
      references LoaiMB (MaLoaiMB)
      on update restrict
      on delete restrict;

alter table MayBay
   add constraint FK_MAYBAY_D_LOAIMB foreign key (MaLoaiMB)
      references LoaiMB (MaLoaiMB)
      on update restrict
      on delete restrict;

