﻿if not exists(select * from sys.databases where name = 'TestAirStar')
    create database TestAirStar

use TestAirStar

create table LoaiMB (
	MaLoaiMB varchar(10) primary key,
	TenLoai varchar(30)
)

create table MayBay (
	SoHieuMB varchar(10) primary key,
	MaLoaiMB varchar(10),
	Ten varchar(30),
	DungTich int,
	constraint fk_MaLoaiMB foreign key (MaLoaiMB) references LoaiMB(MaLoaiMB)
)

create table NhanVien (
	MaNV varchar(10) primary key,
	Ho nvarchar(20),
	Ten nvarchar(20),
	Luong decimal(19, 4),
	DiaChi varchar(50),
	constraint chk_MaNV check (MaNV like 'PC%' or MaNV like 'TV%' or MaNV like 'PV%')
)

create table GiayChungNhan (
	MaGCN varchar(10) primary key,
	MaPhiCong varchar(10) unique,
	MaLoaiMB varchar(10),
	constraint fk_MaPhiCong foreign key (MaPhiCong) references NhanVien(MaNV),
	constraint fk_MaLoaiMayBay foreign key (MaLoaiMB) references LoaiMB(MaLoaiMB)
)

create table ChuyenBay (
	SoHieuCB varchar(10) primary key,
	DiemXuatPhat nvarchar(30),
	DiemDen nvarchar(30),
	KhoangCach decimal(19, 4),
	GioKhoiHanh datetime,
	GioHaCanh datetime,
	SoHieuMB varchar(10),
	MaPhiCong varchar(10),
	constraint chk_MaPhiCong check (MaPhiCong like 'PC%'),
	constraint fk_SoHieuMB foreign key (SoHieuMB) references MayBay(SoHieuMB),
	constraint fk_MaPC foreign key (MaPhiCong) references NhanVien(MaNV)
)


-- insert LoaiMB
insert into LoaiMB values ('L1', 'Loai 1')
insert into LoaiMB values ('L2', 'Loai 2')
insert into LoaiMB values ('L3', 'Loai 3')

-- insert MayBay
insert into MayBay values ('A1', 'L1', 'Boeing1', 5000)
insert into MayBay values ('A2', 'L1', 'Boeing2', 5000)
insert into MayBay values ('A3', 'L2', 'Boeing3', 4000)
insert into MayBay values ('A4', 'L2', 'Boeing4', 4000)
insert into MayBay values ('A5', 'L3', 'Boeing5', 7000)
insert into MayBay values ('A6', 'L3', 'Boeing6', 7000)

-- insert NhanVien
insert into NhanVien values ('PC1', N'Nguyễn', N'Lâm', 10000000, 'HCM')
insert into NhanVien values ('PC2', N'Trần', N'Vĩnh', 10000000, 'HCM')
insert into NhanVien values ('PC3', N'Nguyễn', N'Huy', 10000000, 'LA')
insert into NhanVien values ('PC4', N'Trần', N'Tín', 10000000, 'LA')
insert into NhanVien values ('TV1', N'Nguyễn', N'Lan', 8000000, 'HCM')
insert into NhanVien values ('TV2', N'Lê', N'Thúy', 8000000, 'HCM')
insert into NhanVien values ('TV3', N'Trần', N'Như', 8000000, 'HN')
insert into NhanVien values ('PV1', N'Nguyễn', N'Tiên', 5000000, 'LA')
insert into NhanVien values ('PV2', N'Trần', N'Tâm', 5000000, 'HN')
insert into NhanVien values ('PV3', N'Lê', N'Kiều', 5000000, 'HCM')

-- insert GiayChungNhan
insert into GiayChungNhan values ('M1', 'PC1', 'L1')
insert into GiayChungNhan values ('M2', 'PC2', 'L2')
insert into GiayChungNhan values ('M3', 'PC3', 'L3')

-- insert ChuyenBay
insert into ChuyenBay values ('C1', 'HCM', 'Ha Noi', 1500, '2016-01-01 07:30:00', '2016-01-01 08:30:00', 'A1', 'PC1')
insert into ChuyenBay values ('C2', 'Ha Noi', 'HCM', 1500, '2016-01-02 13:30:00', '2016-01-02 15:30:00', 'A2', 'PC2')
insert into ChuyenBay values ('C3', 'Ha Noi', 'Hue', 750, '2016-01-02 09:30:00', '2016-01-02 10:30:00', 'A3', 'PC2')
insert into ChuyenBay values ('C4', 'Hue', 'Ha Noi', 750, '2016-01-02 18:30:00', '2016-01-02 19:30:00', 'A4', 'PC2')
insert into ChuyenBay values ('C5', 'HCM', 'Hue', 750, '2016-01-03 10:30:00', '2016-01-02 11:30:00', 'A5', 'PC3')
insert into ChuyenBay values ('C6', 'Hue', 'HCM', 750, '2016-01-04 14:30:00', '2016-01-02 15:30:00', 'A6', 'PC3')

-- update
update NhanVien
set DiaChi = 'Hue' where MaNV = 'PV3'

update MayBay
set DungTich = 6000 where SoHieuMB = 'A2'

update ChuyenBay
set GioKhoiHanh = '2016-01-03 11:30:00', GioHaCanh = '2016-01-02 12:30:00'
where SoHieuCB = 'C5'

-- delete nhan vien co MaNV = PC4
delete from NhanVien
where MaNV = 'PC4'

-- tim nhung phi cong trong danh sach nhan vien
select * from NhanVien
where MaNV like 'PC%'

-- tim nhung may bay ma phi cong PC1 co the lai
select * from MayBay where MaLoaiMB in (
	select MaLoaiMB from LoaiMB where MaLoaiMB in (
		select MaLoaiMB from GiayChungNhan
		where MaPhiCong = 'PC1'
	)
)

-- tim nhung nhan vien co dia chi tai HCM
select MaNV, Ho, Ten from NhanVien
where DiaChi = 'HCM'

-- tim nhung chuyen bay khoi hanh ngay 02/01/2016
select SoHieuCB, DiemXuatPhat, DiemDen, KhoangCach, SoHieuMB, MaPhiCong from ChuyenBay
where year(GioKhoiHanh) = 2016 and month(GioKhoiHanh) = 1 and day(GioKhoiHanh) = 2

-- rule 
create rule Luong_rule
as
@Luong < 15000000 and @Luong >= 1000000

sp_bindrule Luong_rule, 'NhanVien.Luong'

-- check
alter table ChuyenBay add constraint check_GioBay check (datepart(hour, GioKhoiHanh) < datepart(hour, GioHaCanh))

-- procedure
create procedure luongSauBaoHiem @MaNV varchar(10)
as
update NhanVien
set Luong = Luong - 200000 where MaNV = @MaNV
select MaNV, Ho, Ten, Luong as LuongSauBaoHiem, DiaChi from NhanVien
where MaNV = @MaNV
go

exec luongSauBaoHiem @MaNV = 'PC1'

-- function
create function showPhucVu (@ChucVu varchar(10))
returns table
as
	return (select * from NhanVien where MaNV like @ChucVu + '%')
go

select * from showPhucVu('PV') as DanhSachPhucVu


-- trigger 
create trigger check_phi_cong
on ChuyenBay
for insert
as
begin
declare @tempMaPC varchar(10), @tempMaLoaiMB varchar(10)
set @tempMaLoaiMB = (
	select MayBay.MaLoaiMB from MayBay, inserted
	where MayBay.SoHieuMB = inserted.SoHieuMB
)
set @tempMaPC = (
	select GiayChungNhan.MaPhiCong
	from GiayChungNhan, inserted
	where GiayChungNhan.MaLoaiMB = @tempMaLoaiMB and GiayChungNhan.MaPhiCong = inserted.MaPhiCong
)
if (@tempMaPC is null)
	begin
	print 'Phi cong khong duoc lai chuyen bay nay'
	rollback tran
	end
end
