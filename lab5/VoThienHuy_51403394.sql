create database quanlythuctap;
use quanlythuctap;

CREATE TABLE Sinhvien (
    masv VARCHAR(20) PRIMARY KEY,
    ten VARCHAR(20),
    quequan VARCHAR(50),
    ngaysinh DATETIME,
    hocluc INTEGER
);

CREATE TABLE Detai (
    madt VARCHAR(20) PRIMARY KEY,
    tendt VARCHAR(20),
    cn VARCHAR(20),
    kp INTEGER
);

CREATE TABLE Sinhvien_Detai (
    masv VARCHAR(20),
    madt VARCHAR(20),
    nthuctap VARCHAR(50),
    quangduong INT,
    ketqua VARCHAR(20)
);

alter table Sinhvien_Detai add constraint FK_Sinhvien_Detai_masv_Sinhvien foreign key(masv) references Sinhvien(masv);
alter table Sinhvien_Detai add constraint FK_Sinhvien_Detai_madt_Detai foreign key(madt) references Detai(madt);

alter table Sinhvien_Detai add check(ketqua >= 0 and ketqua <= 10);
alter table Sinhvien add check(hocluc >= 0 and hocluc <= 10);
alter table Detai add check(kp < 100000);

alter table Detai alter column kp set default 0;


insert into Sinhvien values ('001', 'Huy', 'HCM', '1996-1-1', 10);
insert into Sinhvien values ('002', 'Hien', 'HCM', '1996-1-2', 9);
insert into Sinhvien values ('003', 'Thuy', 'HCM', '1996-1-3', 8);
insert into Sinhvien values ('004', 'An', 'HCM', '1996-1-4', 7);
insert into Sinhvien values ('005', 'Lan', 'HCM', '1996-1-5', 6);

insert into Detai values ('s1', 'cntt', 'aaa', 500000);
insert into Detai values ('s2', 'khud', 'aaa', 300000);
insert into Detai values ('s3', 'ds', 'bbb', 400000);
insert into Detai values ('s4', 'ktmt', 'bbb', 600000);
insert into Detai values ('s5', 'cnhh', 'aaa', 200000);


insert into Sinhvien_Detai values ('001', 's1', 'Lam Dong', 15, 10);
insert into Sinhvien_Detai values ('002', 's2', 'Da Nang', 14, 9);
insert into Sinhvien_Detai values ('003', 's1', 'HCM', 10, 8);
insert into Sinhvien_Detai values ('004', 's3', 'HCM', 17, 7);
insert into Sinhvien_Detai values ('005', 's4', 'HCM', 25, 6);

CREATE VIEW cau2_a AS
    (SELECT 
        *
    FROM
        Sinhvien
    WHERE
        hocluc > 8.5
            AND 2016 - YEAR(ngaysinh) < 20);
SELECT 
    *
FROM
    cau2_a;

CREATE VIEW cau2_b AS
    (SELECT 
        *
    FROM
        Detai
    WHERE
        kp > 1000000);
SELECT 
    *
FROM
    cau2_b;

CREATE VIEW cau2_c AS
    (SELECT 
        a.masv, a.ten, a.ngaysinh, a.hocluc, b.ketqua
    FROM
        Sinhvien a,
        Sinhvien_Detai b
    WHERE
        a.masv = b.masv AND hocluc > 8
            AND ketqua > 8
            AND 2016 - YEAR(ngaysinh) < 20);
SELECT 
    *
FROM
    cau2_c;

CREATE VIEW cau2_d AS
    (SELECT 
        a.cn
    FROM
        Detai a,
        Sinhvien_Detai b
    WHERE
        a.madt = b.madt
            AND b.masv IN (SELECT 
                *
            FROM
                Sinhvien
            WHERE
                b.masv = Sinhvien.masv
                    AND Sinhvien.quequan = 'HCM'));
SELECT 
    *
FROM
    cau2_d;

CREATE VIEW cau2_e AS
    (SELECT 
        *
    FROM
        Sinhvien
    WHERE
        YEAR(ngaysinh) < 1980
            AND quequan = 'Hai Phong');
SELECT 
    *
FROM
    cau2_e;

CREATE VIEW cau2_f AS
    SELECT 
        AVG(Sinhvien_Detai.ketqua) AS DTB
    FROM
        Sinhvien_Detai,
        Sinhvien
    WHERE
        Sinhvien_Detai.masv = Sinhvien.masv
            AND Sinhvien.quequan = 'HCM';
SELECT 
    *
FROM
    cau2_f;

CREATE VIEW cau2_g AS
    SELECT 
        nthuctap
    FROM
        Sinhvien_Detai
    WHERE
        (madt = 's5')
    GROUP BY nthuctap;

SELECT 
    *
FROM
    cau2_g;

CREATE VIEW cau2_h AS
    SELECT 
        Sinhvien.quequan, COUNT(Sinhvien.quequan) AS soluong
    FROM
        Sinhvien
    GROUP BY Sinhvien.quequan;
SELECT 
    *
FROM
    cau2_h;

--3a
SELECT Sinhvien_Detai.madt
FROM Sinhvien_Detai
GROUP BY Sinhvien_Detai.madt
HAVING COUNT(Sinhvien_Detai.madt) >= 2;

--3b
SELECT 
    *
FROM
    Sinhvien
WHERE
    NOT quequan = 'HCM'
        AND hocluc > ALL (SELECT 
            *
        FROM
            Sinhvien
        WHERE
            quequan = 'HCM');

--3c
SELECT Sinhvien.ten, (CASE WHEN (Sinhvien.hocluc + 2) < 10 THEN Sinhvien.hocluc + 2 ELSE 10 END ) AS hocluc
FROM Sinhvien
WHERE Sinhvien.quequan = 'MT';

--3d
SELECT 
    *
FROM
    Sinhvien a,
    Sinhvien_Detai b
WHERE
    a.masv = b.masv
        AND a.quequan = b.nthuctap;

--3e
SELECT 
    a.madt
FROM
    Detai a
WHERE
    a.madt NOT IN (SELECT 
            b.madt
        FROM
            Sinhvien_Detai b)
GROUP BY a.madt;

--3f
SELECT Detai.tendt
FROM Detai
WHERE Detai.madt IN
(SELECT Sinhvien_Detai.madt FROM Sinhvien_Detai
WHERE Sinhvien_Detai.masv IN
(SELECT Sinhvien.masv
FROM Sinhvien
WHERE Sinhvien.hocluc >= ALL (SELECT MAX(Sinhvien.hocluc)
FROM Sinhvien));

--3g
SELECT Detai.tendt
FROM Detai
WHERE Detai.madt NOT IN
(SELECT Sinhvien_Detai.madt
FROM Sinhvien_Detai
WHERE Sinhvien_Detai.masv IN
(SELECT Sinhvien.masv
FROM Sinhvien
WHERE Sinhvien.hocluc <= ALL (SELECT MIN(Sinhvien.hocluc)
FROM Sinhvien)));

--3h
SELECT *
FROM Sinhvien
WHERE Sinhvien.masv IN
(SELECT Sinhvien_Detai.masv
FROM Sinhvien_Detai
WHERE Sinhvien_Detai.madt IN
(SELECT Detai.madt
FROM Detai
WHERE Detai.kp > ((SELECT SUM(Detai.kp)
FROM Detai) / 5)));

--3i
SELECT *
FROM Sinhvien
WHERE Sinhvien.hocluc >
(SELECT AVG(Sinhvien_Detai.ketqua)
FROM Sinhvien_Detai
WHERE Sinhvien_Detai.madt = 's5');


























